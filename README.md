# Generate work events

Simple node API

### Implemented features
* localhost:8080/config POST call for configuration injection
* Generation of work events and return with the POST call
* basic payload validation for config POST call
* A few basic utility tests
* Swagger docs (localhost:8080/docs)

### Missing features
* store
* holiday description functionality
* basic user auth for calls
* config update call
* getting specific config from store call
* user login call
* code serve script for prod deployment

### Tech debt created
* there is still generated code, that needs to be cleaned up / replaced / removed
* possibly not every lib included is needed (needs generated code cleanup first)
* addConfig function testability is not great

### To run the app 

npm run start

### To run the tests

npm run test

### To inject configs call:
POST
localhost:8080/config

Example body:

~~~
{
  "name": "Test",
  "surname": "Testersen",
  "periodStartTimestamp": "2017-04-01T00:00:00.000Z",
  "periodEndTimestamp": "2017-05-31T00:00:00.000Z",
  "countryCode": "EE",
  "holidays": [{
    "description": "PTO",
    "startTimestamp": "2017-05-10T00:00:00.000Z",
    "endTimestamp": "2017-05-20T00:00:00.000Z"
  }]
}
~~~

Example response

~~~
{
  "name": "Test",
  "surname": "Testersen",
  "countryCode": "EE",
  "list": [
    {
      "date": "2017-04-01T00:00:00.000Z",
      "working": false
    },
    {
      "date": "2017-04-02T00:00:00.000Z",
      "working": false
    },
    ...
  ]
}
~~~
