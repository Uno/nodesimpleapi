import {assert, expect} from 'chai';
import { isInBetweenHolidays } from '../src/api/utils/dateUtils'
import {generateDayList} from '../src/api/services/config';

const holidayFixture = [{
    "description": "PTO",
    "startTimestamp": "2017-05-10T00:00:00.000Z",
    "endTimestamp": "2017-05-20T00:00:00.000Z"
  }];

const configFixture = {
  name: "Test",
  surname: "Testersen",
  periodStartTimestamp: "2017-05-01T00:00:01.000Z",
  periodEndTimestamp: "2017-05-10T00:00:00.000Z",
  countryCode: "EE",
  holidays: [{
    description: "PTO",
    startTimestamp: "2017-05-02T00:00:00.000Z",
    endTimestamp: "2017-05-05T00:00:00.000Z"
  }]
};

describe('Config tests', () => {
  describe('Check date if between to dates', () => {
    it('should say false', () => {
      const workingDay = "2017-05-05T00:00:00.000Z";
      const result = isInBetweenHolidays(holidayFixture, workingDay);
      assert.equal(result, false);
    });
    it('should say true', () => {
      const holiday = "2017-05-15T00:00:00.000Z";
      const result = isInBetweenHolidays(holidayFixture, holiday);
      assert.equal(result, true);
    });
  });
  describe('Check if list generated', () => {
    it('should create list', () => {
      const result = generateDayList(
        configFixture.periodStartTimestamp,
        configFixture.periodEndTimestamp,
        configFixture.countryCode,
        configFixture.holidays);
      expect(result).to.be.an('array');
      expect(result.length).to.be.equal(9);
    });
  });
});
