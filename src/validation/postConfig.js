export const postConfigExpected = {
  "name": "",
  "surname": "",
  "periodStartTimestamp": "",
  "periodEndTimestamp": "",
  "countryCode": "",
  "holidays": [{}]
};

export const requiredConfigArray = [
  "name",
  "surname",
  "periodStartTimestamp",
  "periodEndTimestamp",
  "countryCode",
  "holidays"
];
