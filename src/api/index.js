import express from 'express';
import * as fs from 'fs';
import * as path from 'path';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import swaggerTools from 'swagger-tools';
import swaggerUi from 'swagger-ui-express';
import swaggerDocument from '../../docs/swagger.json';
import config from '../lib/config';
import logger from '../lib/logger';

const log = logger(config.logger);
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

/*
 * Routes
 */
app.use('/config', require('./routes/config'));
app.use('/user', require('./routes/user'));
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// catch 404
app.use((req, res, next) => {
  log.error(`Error 404 on ${req.url}`);
  res.status(404).send({ status: 404, error: 'Not found' });
});

// catch errors
app.use((err, req, res, next) => {
  const status = err.status || 500;
  log.error(`Error ${status} (${err.message}) on ${req.method} ${req.url} with payload ${req.body}.`);
  res.status(status).send({ status, error: 'Server error' });
});

// add swagger
const spath = path.resolve('./docs/swagger.json');
const file = fs.readFileSync(spath, 'utf8');
const spec = JSON.parse(file);

const options = {
  swaggerUi: './docs/swagger.json',
  controllers: './src/api/routes',
  useStubs: false,
};

// swagger middleware in express
swaggerTools.initializeMiddleware(spec, (middleware) => {
  app.use(middleware.swaggerMetadata());
  app.use(middleware.swaggerValidator());
  app.use(middleware.swaggerRouter(options));
  app.use(middleware.swaggerUi());
});

export default app;
