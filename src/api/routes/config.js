import express from 'express';
import payloadChecker from 'payload-validator';
import config from '../services/config';
import { postConfigExpected, requiredConfigArray } from '../../validation/postConfig';

const router = express.Router();

/**
 *
 *
 *
 */
router.post('/', (req, res, next) => {
  const options = {
    body: req.body
  };
  const result = payloadChecker.validator(req.body, postConfigExpected, requiredConfigArray, false);

  if (result.success) {
    config.addConfig(options, (err, data) => {
      if (err) {
        const err_response = {status: 400, message: 'An error has occurred '};
        return res.status(400).send(err_response);
      }
      res.status(200).send(data);
    });
  } else {
    const err_response = {status: 405, message: 'Invalid json'};
    return res.status(405).send(err_response);
  }
});

/**
 *
 *
 *
 */
router.put('/', (req, res, next) => {
  const options = {
    body: req.body
  };

  config.updateConfig(options, (err, data) => {
    if (err) {
      const err_response = { status: 400, message: 'Invalid ID supplied' };
      return res.status(400).send(err_response);
    }
    res.status(200).send(data);
  });
});

/**
 *
 *
 *
 */
router.get('/:id', (req, res, next) => {
  const options = {
    id: req.params.id
  };

  config.getConfigById(options, (err, data) => {
    if (err) {
      const err_response = { status: 400, message: 'Invalid id supplied' };
      return res.status(400).send(err_response);
    }

    res.status(200).send(data);
  });
});

module.exports = router;
