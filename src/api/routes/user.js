import express from 'express';
import user from '../services/user';

const router = express.Router();

/**
 * 
 *
 * 
 */
router.get('/login', (req, res, next) => {
  const options = {
    username: req.query.username,
    password: req.query.password
  };

  user.loginUser(options, (err, data) => {
    if (err) {
      const err_response = { status: 400, message: 'Invalid username/password supplied' };
      return res.status(400).send(err_response);
    }

    res.status(200).send(data);
  });
});

module.exports = router;
