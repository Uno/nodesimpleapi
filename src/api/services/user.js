/**
 * @param {Object} options
 * @param {String} options.username The user name for login
 * @param {String} options.password The password for login in clear text
 * @param {Function} callback
 */
export function loginUser (options, callback) {
  // Implement you business logic here...
}

