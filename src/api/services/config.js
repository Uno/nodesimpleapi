import Holidays from 'date-holidays';
import { isInBetweenHolidays } from '../utils/dateUtils';
/**
 * @param {String} periodStart
 * @param {String} periodEnd
 * @param {String} countryCode
 * @param {Array} holidays
 */
export const generateDayList = (periodStart, periodEnd, countryCode, holidays) => {
  let start = new Date(periodStart);
  const end = new Date(periodEnd);
  const bankHolidays = new Holidays(countryCode);
  const checkIfWorkingDay = () => (start.getDay() !== 6 && start.getDay() !== 0 && !bankHolidays.isHoliday(start));

  const dayArray = [];

  while (start < end) {
    const isWorking = checkIfWorkingDay() && !isInBetweenHolidays(holidays, start);
    dayArray.push({
      date: new Date(start),
      working: isWorking,
    });
    start = new Date(start.setDate(start.getDate() + 1));
  }
  return dayArray;
};

/**
 * @param {Object} options
 * @param {Object} options.body Config object that needs to be added to the store
 * @param {Function} callback
 */
const addConfig = (options, callback) => {
  const payload = options.body;

  const name = payload.name;
  const surname = payload.surname;
  const periodStart = payload.periodStartTimestamp;
  const periodEnd = payload.periodEndTimestamp;
  const countryCode = payload.countryCode;
  const holidays = payload.holidays;

  const list = generateDayList(periodStart, periodEnd, countryCode, holidays);
  const response = {
    name,
    surname,
    countryCode,
    list
  };
  callback(null, response);
};

/**
 * @param {Object} options
 * @param {Object} options.body Config object that needs to be added to the store
 * @param {Function} callback
 */
const updateConfig = (options, callback) => {
  const payload = options.body;
  // Implement you business logic here...
};

/**
 * @param {Object} options
 * @param {Integer} options.id config id
 * @param {Function} callback
 */
const getConfigById = (options, callback) => {
  // Implement you business logic here...
};

export default {
  addConfig,
  updateConfig,
  getConfigById,
}
