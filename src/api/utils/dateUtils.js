export const isInBetweenHolidays = (holidays, checking) => {
  const check = new Date(checking);
  for (let h of holidays) {
    const start = new Date(h.startTimestamp);
    const end = new Date(h.endTimestamp);
    if (start < check && check < end) {
      return true;
    }
  }
  return false;
};
